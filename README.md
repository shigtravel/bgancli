simple information is simple:
```
$ ./bgan.py /dev/ttyACM1
/dev/ttyACM1> temp
temp:  0, 34
/dev/ttyACM1> bat
battery: 0,4
```

Sending AT commands directly:
```
/dev/ttyACM1> AT_ITEMP=?


_ITEMP: (0-4), (-127 - +128)
OK
/dev/ttyACM1>
```

`<3` that the terminal lets us extract the command list with `AT*`
```
/dev/ttyACM1> commands

+CACM                Accumulated call meter
+CAEMLPP             eMLPP Priority Registration and Interrogation
+CAMM                Accumulated call meter maximum
+CBC                 Battery charge
+CCFC                Call forwarding number and conditions
+CCLK                Clock
+CCUG                Closed user group
+CCWA                Call waiting
+CDIP                Called line identification presentation
+CFUN                Set phone functionality
+CGACT               PDP context activate or deactivate
+CGATT               PS attach or detach
+CGCMOD              PDP Context Modify
+CGDATA              Enter data state
+CGDCONT             Define PDP Context
+CGDSCONT            Define Secondary PDP Context
+CGEQMIN             Minimum acceptable Quality of Service
+CGEQNEG             Negotiated Quality of Service
+CGEQREQ             Requested Quality of Service
+CGEREP              Event Reporting
+CGMI                Request manufacturer identification
+CGMM                Request model identification
+CGMR                Request revision identification
+CGPADDR             Show PDP address
+CGQMIN              3G Quality of Service Profile(Minimum acceptable)
+CGQREQ              3G Quality of Service Profile(Requested)
+CGREG               GPRS network registration status
+CGSMS               Select service for MO SMS messages
+CGSN                Request product serial number identification
+CGTFT               Traffic Flow Template
+CHUP                Hangup call
+CIMI                Request international mobile subscriber identity
+CIND                Indicator Control
+CLAC                List all available AT commands
+CLCC                List current calls
+CLCK                Facility lock
+CLIP                Calling line identification presentation
+CLIR                Calling line identification restriction
+CMAR                Master Reset
+CMEE                Report Mobile Termination error
+CMER                Mobile Termination Event Reporting
+CMGC                Send Command
+CMGD                Delete Message
+CMGF                Message Format
+CMGL                List Messages
+CMGR                Read Message
+CMGS                Send Message
+CMGW                Write Message to Memory
+CMMS                More Messages to Send
+CMSS                Send Message from Storage
+CNMA                New Message Acknowledgement to ME/TA
+CNMI                New Message Indications to TE
+CNUM                Subscriber number
+COLP                Connected line identification presentation
+COLR                Connected Line Identification Restriction
+COPN                Read operator names
+COPS                PLMN Selection
+CPAS                Activity status
+CPBR                Read Phonebook Entry
+CPBS                Select Phonebook Storage
+CPBW                Write Phonebook Entry
+CPIN                Enter PIN
+CPLS                Selection of preferred PLMN list
+CPMS                Preferred Message Storage
+CPOL                Preferred PLMN list
+CPPS                eMLPP subscriptions
+CPWD                Change password
+CREG                Network registration
+CRES                Restore Settings
+CRSM                Restricted SIM Access
+CSAS                Save Settings
+CSCA                Service Centre Address
+CSCB                Select Cell Broadcast Message Types
+CSCS                Select TE character set
+CSMP                Set Text Mode Parameters
+CSMS                Select Message Service
+CSSN                Supplementary service notifications
+CSVM                Set Voice Mail Number
+CUSD                Unstructured supplementary service data
+W3GPP               List 3GPP Commands
+WTTBTDISALLOW       BLUE TOOTH DISALLOWED
+WTTBTALLOW          BLUE TOOTH ALLOWED
+WNERADEVINT         NERA Interface Setting
+WNERADEVMNG         NERA Device Port Setting
+WNERADTE            NERA DTE
+WNERAMMSTS          NERA Mobility Management Status
+WNERAPIN            NERA PIN
+WNERASRVSTS         NERA Service Status
+WNERATEST           NERA Test
+WS                  Select wireless network

_IBALARM             UnSolicited reports for Alarm
_IBLTH               Manage BlueTooth Pairing
_IBNOTIFY            Control Unsolicited notifications
_IBTIF               Configure UT BlueTooth Interface
_IBTINQ              Bluetooth device enquiry
_IGPS                Returns PUT GPS Information
_ILOG                Retrive PUT Log files
_IMETER              Call meter for CS & PS calls
_INANTP              Antenna Pointing Operation
_INBTHSPHUP          Terminate headset call
_INBTHSPDIAL         Dial outgoing call with headset
_IGPSINFO            UnSolicited GPS status reports
_INGPSINFO           UnSolicited GPS status reports
_INIPAVAIL           Ethernet/IP-features available
_INIS                Network Interface status
_INMUPSTS            Multi user profile command
_INMUPMNG            Multi user profile command
_INMUPLOGOFF         Multi user profile command
_INMLTCAST           Multicast filtering
_INPDPCNTREL         PDPContext automatic release
_INPING              Ping another computer
_INSETUP             Configure the UT, eg. test settings
_INSIMRDY            Availability of Phone Book & SMS in SIM/USIM
_INSLEEP             Unsolicited reports for Sleep mode
_IPOINT              Set Antena Pointing
_ISIG                Unsolicited reports for Signal Strength
_ITEMP               Temparature Status of PUT
```
