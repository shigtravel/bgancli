#!/usr/bin/env python

import sys, os, fcntl, tty
import itertools
import readline

class TTY(file):
    def __init__(self, name, mode='rb+', buffering=1, *args,**argd):
        file.__init__(self, name, mode, buffering, *args,**argd)
        tty.setraw(self.fileno())
        self.set_blocking(False)
        try:
            self.read(1024)
            if len(self.read(1024)):
               self.close()
               raise RuntimeError('Seems to be in data mode. Not able to deal yet. Need to use +++ break')
        except IOError: pass
        self.set_blocking(True)

    def set_blocking(self,block):
        flags = fcntl.fcntl(self,fcntl.F_GETFL)
        if block:
            flags &= (~os.O_NONBLOCK & 0xffffffff)
        else:
            flags |= os.O_NONBLOCK
        fcntl.fcntl(self, fcntl.F_SETFL, flags)

class ATCommandTTY(TTY):
    """
    AT command aware TTY handler
    """
    RESPONSES = ('OK','ERROR','CONNECT','NO CARRIER','BUSY','NO ANSWER')
    def __init__(self,filename, init='ATZ'):
        TTY.__init__(self, filename)
        self.at()
        self.at()
        self.at_ok()
        if init: self.at_ok(init)
        self.at_ok('ATE0') # Switch off cmd echo

    def get_at_response(self):
        """read until got an OK or ERROR etc
        returns an iterator over lines
        """
        while True:
            l = self.readline()
            if l == "\r": continue  # dangling \r
            #print >> sys.stderr, "<<<",l,
            yield l
            if l.strip() in self.RESPONSES: return
    def cmd(self, cmd):
        '''send an at command and read until getting an OK or ERROR
        e.g. print "".join(tty.at('ATI'))
        '''
        self.write(cmd)
        self.write("\r\n")
        self.flush()
        #print >> sys.stderr, ">>>",cmd
        return self.get_at_response()
    def at(self,cmd='AT'):
        '''send an AT command, buffer the response, and return
        ("OK" or "ERROR", [responselines])

        just a blocking wrapper for cmd.
        '''
        l = map(str.strip,self.cmd(cmd))
        return l[-1],l[:-1]
    def at_ok(self, cmd='AT'):
        '''executes an AT command and returns an iterator over the output
        output is stripped and an exception is raised if the response is not 'OK'.
        '''
        ok,resp = self.at(cmd)
        if ok != 'OK':
            raise IOError('Response to '+cmd+' was not OK')
        return resp
    def get_at_var(self, var):
        '''e.g. bgan_cli.get_at_var('AT+CBC')'''
        for l in self.at_ok('AT'+var):
            key,_,val = l.strip().partition(': ')
            if _ == ': ' and key == var:
                return val
    def get_identity(self):
        out = []
        map(out.extend, [self.at('ATI%s' % n)[1] for n in range(10)] )
        return out

class BGANTTY(ATCommandTTY):
    def __init__(self, *args, **argd):
        ATCommandTTY.__init__(self,*args,**argd)
        self.commands = {}

        # prefill a list of commands
        for l in self.get_commands():
            if l == '': continue
            key,_,val = l.partition(' ')
            self.commands[key] = val

    def has_command(self, command):
        return self.commands.has_key(command)

    def get_commands(self):
        return self.at_ok('AT*')

    def get_at_var(self,var):
        if self.has_command(var): 
            return ATCommandTTY.get_at_var(self, var)
        raise NotImplemented

    def get_battery(self): return self.get_at_var('+CBC')

    def get_temp(self): return self.get_at_var('_ITEMP')


class BGANCLI(BGANTTY):
    def help(self):
        print '? help exit quit commands temp battery id, identity '
        print
        print 'You can also type AT commands directly, e.g.: ATI'

    def do_cmd(self, *args):
        if len(args) == 0:
            self.help()
            return
        cmd,args = args[0],args[1:]
        lcmd = cmd.lower()

        if lcmd in ('?','help','-h','--help'):
            self.help()
        elif lcmd == 'temp':
            print "temp:",self.get_temp()
        elif lcmd == 'commands':
            print "\n".join(self.get_commands())
        elif lcmd in ('bat','battery'):
            print "battery:",self.get_battery()
        elif lcmd in ('id','identity'):
            print " ".join(self.get_identity())
        elif lcmd in ('quit','exit'):
            raise SystemExit
        elif lcmd[:2] == 'at':
            for l in self.cmd(cmd):
                print l.strip()

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print >>sys.stderr, sys.argv[0],"<device name>\n\ne.g. ",sys.argv[0],"/dev/ttyACM1"
        exit(1)

    port = sys.argv[1]
    bgan_cli = BGANCLI(port)

    while True:
        try:
            inp = raw_input(port + '> ').split()
        except NotImplemented:
            print "Not implemented"
        except EOFError:
            break
        bgan_cli.do_cmd(*inp)
