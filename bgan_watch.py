#!/usr/bin/env python
import time
import sys, os, fcntl, tty
import itertools
import readline

from bgan import *

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print >>sys.stderr, sys.argv[0],"<device name>\n\ne.g. ",sys.argv[0],"/dev/ttyACM1"
        exit(1)

    port = sys.argv[1]
    print "waiting for bgan terminal to wake up on",port
    while True:
        try:
            bgan_cli = BGANCLI(port)
            break
        except: 
            time.sleep(0.1)
    print "Awake!"


    print "Dumping error log:"
    for l in bgan_cli.at_ok('AT_ILOG="error"'): print l
    
    print "Dumping event log:"
    for l in bgan_cli.at_ok('AT_ILOG="event"'): print l

    print "Current alarms:"
    for l in bgan_cli.at_ok('AT_ILOG="active_alarms"'): print l

    print "Switching on all unsolicited reports and listening"
    for l in bgan_cli.at_ok('AT_IBNOTIFY="ALL",1'): print l


    for l in bgan_cli:
        print strip(l)
